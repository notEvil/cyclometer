# Cyclometer

is a project based on [*Bangle.js*](https://www.espruino.com/Bangle.js) and [*PHONK*](https://github.com/victordiaz/PHONK) aiming to provide detailed information about bicycle tours. In its current state, it is able to record high frequency data on *Bangle.js* and Android.

With this data, the goal is to compute and display (in addition to the usual metrics)
- slope
- cadence
- gear
- track properties (e.g. smoothness)
- …

and provide the means to perform data analysis.

![Bike](./images/bike.jpg){height=375px} ![PHONK](./images/PHONK.jpg){height=375px} ![Bangle.js](./images/Bangle.js.jpg){width=160px}

## How

The idea is simple: attach *Bangle.js* to the frame of your bike and put your phone in your trouser pocket. The fixed position of *Bangle.js* provides relatively distinct information about the overall motion while your phone is picking up your leg activity.

## Quickstart

**Prepare** (only once)

- Build the firmware for *Bangle.js* with `python ./Bangle.js/Espruino/build.py`
    - see [./Bangle.js/Espruino](./Bangle.js/Espruino) for more information
    - it will fetch the source code from [https://github.com/espruino/Espruino](https://github.com/espruino/Espruino), apply changes from [https://github.com/notEvil/Espruino](https://github.com/notEvil/Espruino) and create a `.zip` file in the current directory
    - if it doesn't work on first try, delete `./Bangle.js/Espruino/Espruino` and retry
- Upload the firmware to *Bangle.js*
    - see [http://www.espruino.com/Firmware+Update#nrf52](http://www.espruino.com/Firmware+Update#nrf52)
    - e.g.
        - upload the `.zip` file to Android
        - "Long-press `BTN1`+`BTN2` for about 6 seconds until the screen goes blank"
        - "Release `BTN2`"
        - "Release `BTN1` a moment later while `====` is going across the screen"
        - use [nRF Device Firmware Update](https://play.google.com/store/apps/details?id=no.nordicsemi.android.dfu), select the `.zip` file and device `DfuTarg`
- Install the *Bangle.js* app
    - see [./Bangle.js](./Bangle.js) for more information
    - in Google Chrome, navigate to [https://www.espruino.com/ide](https://www.espruino.com/ide)
    - press connect button at the top left, then `status`
    - follow the instructions to set up Web Bluetooth for Google Chrome
    - restart Google Chrome
    - connect to *Bangle.js* and upload
        - `./Bangle.js/app/cyc.app.js`
        - `./Bangle.js/app/cyc.img`
        - `./Bangle.js/app/cyc.info`
        - `./Bangle.js/app/rack.boot.js`
        - `./Bangle.js/app/rack.js` **as `rack` !!**
    - soft reset *Bangle.js* (long press `BTN3`)
- Build *PHONK* with `python ./PHONK/build.py`
    - see [./PHONK](./PHONK) for more information
    - it will fetch the source code from [https://github.com/victordiaz/PHONK](https://github.com/victordiaz/PHONK), apply changes from [https://github.com/notEvil/PHONK](https://github.com/victordiaz/PHONK), prepare the Android Studio project and start Android Studio
    - if it doesn't work on first try, delete `./PHONK/PHONK` and retry
    - in Android Studio
        - follow the instructions to set up Android SDK
        - wait for background tasks to finish
        - find the tool window `Build Variants` and select `normalRelease` (requires at least one debug build in the past)
        - make project
    - close Android Studio
    - now there should be a `.apk` file in the current directory
- Install *PHONK* on Android using the `.apk` file
- Install the *PHONK* app
    - copy all directories in `./PHONK/app/` to `/path/to/phonk_io/playground/User Projects/`

**Run**

- Start `Cyclometer` on *Bangle.js*
- Start *PHONK* on Android
- Find and start `cyclometer_service` in *PHONK*
- Find and start `cyclometer` in *PHONK*
- Press "Wake" to create a wake lock (red: not locked, green: locked)
- Press "Start" to start recording
- Press `BTN1` to start recording on *Bangle.js*
- Do your thing
    - lock your phone so it won't waste energy / unintentionally stop recording
- Long press `BTN1` until it stops recording
- Press "Stop" on Android
- Press "Wake" to remove the wake lock

Each session is written to `/path/to/phonk_io/playground/User Projects/cyclometer_service/`, and depending on connection stability between *Bangle.js* and Android there is a corresponding file (`StorageFile`) on *Bangle.js*. Press "Collect" on Android to transfer those files. Alternatively download those files using the Espruino Web IDE.

## Not what you need?

This project is quite flexible thanks to [*Espruino*](https://github.com/espruino/Espruino) and *PHONK* for the most part. It is easy to
- remove parts like "recording on *Bangle.js*" (=> Android only) or "recording on Android" (=> *Bangle.js* only)
- adjust parts, e.g. to support [Bangle.js 2](https://www.espruino.com/Bangle.js2) or record with higher/lower frequency
- add parts like additional sensors or live analysis

Primarily consider [PHONK/app/cyclometer/main.js](./PHONK/app/cyclometer/main.js), [PHONK/app/cyclometer_service/main.js](./PHONK/app/cyclometer_service/main.js) and  [Bangle.js/app/cyc.app.js](./Bangle.js/app/cyc.app.js) if you want to change something.

## Troubleshooting

- If anything breaks, soft reset *Bangle.js* (long press `BTN3`) and force stop *PHONK* on Android to start clean
- If Android won't connect to *Bangle.js* you probably need to forget and pair *Bangle.js* with Android and then switch Bluetooth off and on

## Special thanks

- to Gordon Williams, the inventor of *Espruino*, for his support on numerous issues and always being friendly and positive!
- to Victor Diaz, the author of *PHONK*, for considering changes to the project and also being positive all the way!
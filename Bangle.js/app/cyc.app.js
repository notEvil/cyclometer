/* uncomment this when using the emulator *
Bangle.getPollHistory = function() { return {acc: [], accIdx: 0, mag: [], magIdx: 0}; };
var Rack = {Session: function() {}};
var _req = require;
function require(name) {
  if (name == 'rack') return {sessions: [], write: function() {}};
  return _req(name);
}
/*  */

function pass() {}

var ACC = 1;
var MAG = 2;
var GPS = 3;
var TIM = 4;
var BUFS = 1000;

var bufs = [];
var buf = new ArrayBuffer(BUFS);
var bufi = 2;  // id == 0
var file = require('Storage').open('cyc' + (new Date()).toISOString().substring(0, 19), 'w');

var get_id = (b, o) => (new DataView(b, o)).getUint16(0);

function get_dv(len) {
  if (buf.length < (bufi + len)) {
    var id = get_id(buf);
    bufs.push(buf);
    if (bufs.length == 11) {
      file.write(btoa(bufs[0]));
      bufs.shift();
    }
    buf = new ArrayBuffer(BUFS);
    (new DataView(buf)).setUint16(0, id + 1);
    bufi = 2;
  }
  var dv = new DataView(buf, bufi);
  bufi += len;
  return dv;
}

function on_accel(a) {
  var v = get_dv(7);
  v.setUint8(0, ACC);
  v.setInt16(1, a.x);
  v.setInt16(3, a.y);
  v.setInt16(5, a.z);
}

function on_mag(m) {
  var v = get_dv(7);
  v.setUint8(0, MAG);
  v.setInt16(1, m.x);
  v.setInt16(3, m.y);
  v.setInt16(5, m.z);
}

var acci;
var magi;

function on_poll() {
  var h = Bangle.getPollHistory(acci, magi);
  h.acc.forEach(on_accel);
  h.mag.forEach(on_mag);
  acci = h.accIdx;
  magi = h.magIdx;
}

function on_gps(g) {
  if (g.fix == 0) return;
  var v = get_dv(26);
  v.setUint8(0, GPS);
  v.setInt32(1, g.lat * 11930464);
  v.setInt32(5, g.lon * 11930464);
  v.setUint16(9, clamp(g.alt * 16, 0, 65535));
  v.setFloat64(11, g.time.getTime());
  v.setUint8(19, g.fix);
  v.setUint8(20, clamp(g.hdop * 12, 0, 255));  // (0, ~21) / 0.083
  v.setUint8(21, g.satellites);
  v.setUint16(22, g.course * 182);
  v.setUint16(24, clamp(g.speed * 327, 0, 65535));  // (0, ~200) / 0.003 km/h
}

var clamp = (a, mi, ma) => a < mi ? mi : (ma < a ? ma : a);

var stat = {};

function draw() {
  g.reset();
  var x = 0, y = 24, w = 240, h = 38;  // 7 + 24 + 7

  var a = [];
  for (var name in stat) a.push(name, ': ', stat[name], ' ');
  a.pop();

  g.setColor(7 < stat.Que || 89 < stat.Mem ? '#f00' : (1 < stat.Que || 79 < stat.Mem ? '#f80' : '#0f0'));
  g.setFont('Vector', 24);
  g.clearRect(x, y, w - 1, y + h - 1);
  g.drawString(''.concat.apply('', a), x + 7, y + 7);
}

function log(s, skip) {
  Terminal.println(s);
  if (!skip) {
    g.clearRect(0, 0, 239, 23);
    Bangle.drawWidgets();
    draw();
  }
}

g.clear();
Bangle.loadWidgets();

log('             # Cyclometer #\n\npress top button to start\nor hold bottom button to exit');

var rack = require('rack');
var session = Rack.Session(1, (a) => {
  if (bufs.length && get_id(a.buffer, a.byteOffset) == get_id(bufs[0])) bufs.shift();
  if (bufs.length) Rack.write(session, new Uint8Array(bufs[0]), pass);
  Rack.write(session, new Uint8Array(1), pass);
}, rack.write.bind(rack));
rack.sessions[1] = session;

Bangle.setGPSPower(true, 'cyc');

function ready(g) {
  if (g.fix) {
    Bangle.removeListener('GPS', ready);
    Bangle.buzz(500);
    delete stat.Sat;
    log('GPS ready');
  } else if (g.satellites != stat.Sat) {
    stat.Sat = g.satellites;
    draw();
  }
}
Bangle.on('GPS', ready);

setWatch(() => {
  log('starting ...', true);
  Bangle.setPollInterval(72);
  Bangle.setOptions({btnLoadTimeout: 0, wakeOnTwist: false});
  Bangle.setCompassPower(true, 'cyc');
  var h = Bangle.getPollHistory(0, 0);
  acci = h.accIdx;
  magi = h.magIdx;
  var pint = setInterval(on_poll, 80);
  Bangle.on('GPS', on_gps);

  var tint = setInterval(() => {
    var v = get_dv(9);
    v.setUint8(0, TIM);
    v.setFloat64(1, getTime());
  }, 60 * 1000);

  var lint = setInterval(() => {
    var m = process.memory(); // TODO don't GC
    stat.Que = bufs.length;
    stat.Mem = Math.floor((m.usage / m.total) * 100) + '%';
    draw();
  }, 5 * 1000);

  log('started\nhold top button to stop');

  var cnt;
  var cto;
  var bwat = setWatch((e) => {
    if (e.state) {
      cnt = 3;
      cto = setTimeout(stop, 1000);
      Bangle.buzz(300);
    } else if (cto) {
      cto = clearTimeout(cto);
    }
  }, BTN1, {repeat: true, edge: 'both'});

  function stop() {
    cnt--;
    if (cnt == 0) {
      log('stopping ...', true);
      Bangle.setGPSPower(false, 'cyc');
      Bangle.setPollInterval(80);
      Bangle.setOptions({btnLoadTimeout: 1500, wakeOnTwist: true, powerSave: true});
      Bangle.setCompassPower(false, 'cyc');
      clearInterval(pint);
      Bangle.removeListener('GPS', on_gps);
      clearInterval(tint);
      clearInterval(lint);
      clearWatch(bwat);

      if (1 < bufs.length) {
        bufs.push(buf);
        for (var b of bufs) file.write(btoa(b));
        bufs.length = 0;
      }

      log('stopped\nhold bottom button to exit');

    } else {
      cto = setTimeout(stop, 1000);
      Bangle.buzz(300);
    }
  }
}, BTN1);

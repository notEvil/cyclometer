exports.has_service = function(sid, cid) {
  var svc = {};
  svc[cid] = {};
  var svcs = {};
  svcs[sid] = svc;
  try {
    NRF.updateServices(svcs);
  } catch (e) {
    return false;
  }
  return true;
}

exports.sessions = [];

exports.write = function(a) {
  NRF.updateServices({0xf011: {0xf013: {value: a, notify: true}}});
}

setTimeout(() => {
  if (require('rack').has_service(0xf011, 0xf012)) return;

  NRF.setServices({
    0xf011: {
      0xf012: {
        maxLen: 53,
        writable: true,
        onWrite: (e) => Rack.read(new Uint8Array(e.data), require('rack').sessions)
      },
      0xf013: {
        maxLen: 53,
        notify: true
      }
    }
  });
}, 5 * 1000);

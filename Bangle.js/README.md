# [*Bangle.js*](https://www.espruino.com/Bangle.js)

is a great device running an amazing Javascript engine called [*Espruino*](https://github.com/espruino/Espruino). Check it out!

This project requires a custom version of *Espruino* because

1. the official version doesn't provide consistent access to accelerometer readings unless the time budget between readings is never fully spent. This project can't provide this guarantee (and I would argue that its a tall order). See [https://github.com/espruino/Espruino/issues/2319](https://github.com/espruino/Espruino/issues/2319) for the motivation and stances.

2. this project uses [*Rack*](../rack) implemented as C library.

For this reason it doesn't make sense to add the *Bangle.js* app to the official Bangle App Loader. The only downside is a more complex install/update process which is insignificant compared to building/uploading the custom firmware.

## App details

**First GPS fix**

The app turns on GPS immediately. On first GPS fix, a buzz indicates that a recording session may start. You can start recording prior to that, but it might take a while for GPS to appear.

**Recording behavior**

While recording, wake on twist and soft reset on long press `BTN3` are disabled. Instead, you need to press any button to wake up temporarily and hold `BTN1` for 3 seconds to stop recording.

**Disconnect behavior**

When the connection to Android breaks, array buffers are enqueued up to a queue length of 10. If the queue is full, the oldest array buffer is removed and written to a `StorageFile`. This is not an issue insofar as each array buffer contains an id which may be used to reconstruct the sequence of array buffers.

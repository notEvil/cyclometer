# [*Espruino*](https://github.com/espruino/Espruino)

## Consistent accelerometer readings

See [https://github.com/espruino/Espruino/issues/2319](https://github.com/espruino/Espruino/issues/2319) for the motivation and stances. The corresponding code is [here](https://github.com/notEvil/Espruino/tree/poll_history).

This version of *Espruino* adds arrays for accelerometer and magnetometer readings to the C library for *Bangle.js* and exposes the function `Bangle.getPollHistory(accIdx, magIdx)`. The poll handler (`peripheralPollHandler` in `libs/banglejs/jswrap_bangle.c`; called by an interrupt) adds readings to the arrays and the call `Bangle.getPollHistory(accIdx, magIdx)` returns
```javascript
{
  acc: [
    {x: number, y: number, z: number},
  ],
  accIdx: number,
  mag: [
    {x: number, y: number, z: number},
  ],
  magIdx: number,
}
```
where
- `acc` contains all readings between argument `accIdx` (inclusive) and the current index (exclusive)
- `accIdx` is the current index
- `mag` contains all readings between argument `magIdx` (inclusive) and the current index (exclusive)
- `magIdx` is the current index

If argument `accIdx` is `-1`, `acc` and `accIdx` are excluded from the result. Argument `magIdx` set to `-1` has a similar effect.

Therefore, to get a consistent sequence of readings
- Call `Bangle.getPollHistory(0, 0)` and remember `.accIdx` and `.magIdx`
- In regular intervals, call `Bangle.getPollHistory` with the current indices, process the arrays and update the indices

## *Rack*

See [*Rack*](../../rack). The corresponding code is [here](https://github.com/notEvil/Espruino/tree/rack).

- This implementation makes heavy use of `#define` to stay close to the Javascript version
- It provides better speed and lower memory requirements

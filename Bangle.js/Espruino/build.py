import pathlib
import subprocess


PATH = pathlib.Path(__file__).parent
FETCH_DEPTH = 10


def run(list, **kwargs):
    print("> {}".format(subprocess.list2cmdline(list)))
    assert subprocess.Popen(list, **kwargs).wait() == 0


espruino_path = PATH / "Espruino"
if not espruino_path.exists():
    _ = "https://github.com/espruino/Espruino"
    run(["git", "clone", "-b", "RELEASE_2V18", "--depth", "1", _])
    _ = ["git", "remote", "add", "notEvil", "https://github.com/notEvil/Espruino"]
    run(_, cwd=espruino_path)

    _ = "refs/tags/RELEASE_2V16:refs/tags/RELEASE_2V16"
    run(["git", "fetch", "--depth", "1", "origin", _], cwd=espruino_path)

    _ = ["git", "fetch", "--depth", str(FETCH_DEPTH), "notEvil", "poll_history"]
    run(_, cwd=espruino_path)
    # run(["git", "cherry-pick", "RELEASE_2V18..FETCH_HEAD"], cwd=espruino_path)
    run(["git", "merge", "FETCH_HEAD"], cwd=espruino_path)

    _ = ["git", "fetch", "--depth", str(FETCH_DEPTH), "notEvil", "rack"]
    run(_, cwd=espruino_path)
    run(["git", "cherry-pick", "RELEASE_2V16..FETCH_HEAD"], cwd=espruino_path)

_ = (
    "source './scripts/provision.sh' BANGLEJS && BOARD=BANGLEJS RELEASE=1"
    " DFU_UPDATE_BUILD=1 make"
)
run(["bash", "-c", _], cwd=espruino_path)

run(["bash", "-c", 'cp "{}/bin/espruino_"*"_banglejs.zip" .'.format(espruino_path)])

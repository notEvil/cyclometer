import pathlib
import subprocess


PATH = pathlib.Path(__file__).parent
FETCH_DEPTH = 10


def run(list, **kwargs):
    print("> {}".format(subprocess.list2cmdline(list)))
    assert subprocess.Popen(list, **kwargs).wait() == 0


phonk_path = PATH / "PHONK"

if not phonk_path.exists():
    _ = "https://github.com/victordiaz/PHONK"
    run(["git", "clone", "-b", "1.5.0", "--depth", "1", _])
    _ = ["git", "remote", "add", "notEvil", "https://github.com/notEvil/PHONK"]
    run(_, cwd=phonk_path)

    _ = ["git", "fetch", "--depth", str(FETCH_DEPTH), "notEvil", "minor23_2"]
    run(_, cwd=phonk_path)
    _ = ["git", "cherry-pick", "3c4024e447bdde2615403eaa3fc3e65005886590..FETCH_HEAD"]
    run(_, cwd=phonk_path)

    with open(PATH / "patch", "rb") as file:
        run(["patch", "-p1"], cwd=phonk_path, stdin=file)

    run(["npm", "run", "cleanAndDeploy"], cwd=phonk_path / "PHONK-examples")

    run(["npm", "i", "@vue/cli-service"], cwd=phonk_path / "PHONK-editor")
    run(["npm", "run", "cleanBuildAndDeploy"], cwd=phonk_path / "PHONK-editor")

    with open(phonk_path / "PHONK-android/signing.properties", "w") as file:
        _ = """
storeFile={}
keyAlias=androiddebugkey
keyPassword=android
storePassword=android
""".strip()
        file.write(_.format(pathlib.Path.home() / ".android/debug.keystore"))

run(["android-studio", str(phonk_path / "PHONK-android")])

_ = phonk_path / "PHONK-android/phonk_app/build/outputs/apk/normal/release"
run(["bash", "-c", 'cp "{}/phonk-"*"-normal-release.apk" .'.format(_)])

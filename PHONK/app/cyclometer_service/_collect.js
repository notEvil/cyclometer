(() => {
  var rack = require('rack');
  var BUFFER_SIZE = 10 * 49;  // ~500

  var file;
  var session = Rack.Session(2, (a) => {
    if (a[0] == 1) {  // list
      Rack.write(session, new Uint8Array(E.toArrayBuffer(JSON.stringify(require('Storage').list(/^cyc/, {sf: true})))), () => { Rack.write(session, new Uint8Array([1]), () => {}); });
    } else {  // start
      file = require('Storage').open(E.toString(a), 'r');
      write();
      write();
    }
  }, (a) => { rack.write(a); });
  function write() {
    if (!file) return;
    var string = file.read(BUFFER_SIZE);
    if (string) {
      Rack.write(session, new Uint8Array(E.toArrayBuffer(string)), write);
    } else {
      var f = file;
      Rack.write(session, new Uint8Array([2]), () => { f.erase(); });
      file = undefined;
    }
  }
  rack.sessions[2] = session;
})();

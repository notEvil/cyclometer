function require(path) { }
require('../lib/phonk')

app.load('../lib/aes.js')
app.load('../lib/base64.js')
app.load('../lib/espruino.js')
app.load('../lib/ipc.js')  // depends on aes, base64
app.load('../lib/javascript.js')
app.load('../lib/rack.js')
app.load('../lib/service.js')
// app.load('../lib/zstd.js')


var PORT = 59954
var ACTIVITY_PORT = 59953
var AES_KEY = '96wySBkrRWF3AsQD+2VHbA=='
var DEVICE_ADDRESS = 'E3:E1:FD:CA:0B:38'


javascript.initialize(true)
service.initialize()


function log(string) {
  service.log(string)
  ipc.send({id: 'log', string: string, source: 'service'}, ACTIVITY_PORT)
}


function onCreate() {  // not called yet
  ipc.start(PORT, base64.decode(AES_KEY), _on_receive)
  log('started')
}


function onDestroy() {  // not called yet
  log('stopped')
  ipc.stop()
}


var wakelock = false
var recording = false
var collecting = false


function _on_receive(object) {
  if (object.source == 'activity') {
    if (object.id == 'get_state') {
      ipc.send({
        id: 'set_state',
        state: {wakelock: wakelock, recording: recording},
        source: 'service',
      }, ACTIVITY_PORT)
      
    } else if (object.id == 'toggle_wakelock') {
      _toggle_wakelock(object.change_id)
      
    } else if (object.id == 'start_recording') {
      _start_recording()
      
    } else if (object.id == 'stop_recording') {
      _stop_recording()
      
    } else if (object.id == 'collect') {
      _collect()
      
    } else if (object.id == 'gps') {
      _gps(object.gps)

    } else if (object.id == 'test') {
      device.vibrate(500)
      util.delay(1e3, () => device.vibrate(500))
    }
  }
}


function _toggle_wakelock(change_id) {
  wakelock = !wakelock
  device.wakeLock(wakelock)
  ipc.send({
    id: 'set_wakelock',
    wakelock: wakelock,
    change_id: change_id,
    source: 'service',
  }, ACTIVITY_PORT)
}


var BUFFER_LENGTH = 100e3
var ACC = 1
var GYR = 2
var GPS = 3
var GRA = 4
var LAC = 5
var ESP = 6
var TIM = 7

var buf = undefined
var bufi = undefined
var bufs = undefined
var output_stream = undefined
// var zstd_output_stream = undefined
var byte_count = undefined
var tim_loop = undefined
var write_loop = undefined


function _get_data_view(length) {
  if (buf.byteLength < (bufi + length)) {
    bufs.push(buf)  // safe because of message queue
    buf = new ArrayBuffer(BUFFER_LENGTH)
    bufi = 0
  }
  var data_view = new DataView(buf, bufi)
  bufi += length
  byte_count += length
  return data_view
}


function _start_recording() {
  if (recording) return
  recording = true

  if (collecting) {
    log('cannot record while collecting')
    recording = false
    return
  }
  
  log('start recording')
  
  buf = new ArrayBuffer(BUFFER_LENGTH);
  bufi = 0
  bufs = []
  output_stream = javascript.get_output_stream((new Date()).toISOString().replace(/:/g, '+'))
  // zstd_output_stream = zstd.get_zstd_output_stream(output_stream)
  var start_date = new Date()
  byte_count = 0
  
  _start_record_local()
  _start_record_espruino()
  
  write_loop = util.loop(5e3, () => {
    while (bufs.length) {
      output_stream.write(javascript.get_java_array(new Int8Array(bufs.shift())))
    }
    
    // zstd_output_stream.flush()
    output_stream.flush()
    
    ipc.send({
      id: 'set_record_progress',
      elapsed_ms: (new Date()) - start_date,
      byte_count: byte_count,
      source: 'service',
    }, ACTIVITY_PORT)
  })
  write_loop.start()
  
  log('started recording')
}


function _stop_recording() {
  if (!recording) return
  recording = false
  
  log('stop recording')
  
  _stop_record_local()
  _stop_record_espruino()
  
  write_loop.stop();
  
  bufs.push(buf)
  buf = new ArrayBuffer(BUFFER_LENGTH)
  bufi = 0
  
  while (bufs.length) {
    output_stream.write(javascript.get_java_array(new Int8Array(bufs.shift())))
  }
  
  // zstd_output_stream.flush()
  // zstd_output_stream.close()
  output_stream.flush()
  output_stream.close()
  
  log('stopped recording')
}


function _start_record_local() {
  log('start recording local')
  
  sensors.accelerometer.onChange((a) => {
    var v = _get_data_view(13)
    v.setFloat32(1, a.x)
    v.setFloat32(5, a.y)
    v.setFloat32(9, a.z)
    v.setUint8(0, ACC)
  })
  
  sensors.gyroscope.onChange((g) => {
    var v = _get_data_view(13)
    v.setFloat32(1, g.x)
    v.setFloat32(5, g.y)
    v.setFloat32(9, g.z)
    v.setUint8(0, GYR)
  })
  
  sensors.location.onChange((l) => {
    var v = _get_data_view(25)
    v.setInt32(1, l.latitude * 11930464)  // (-180, 180) / 8.4e-8 "
    v.setInt32(5, l.longitude * 11930464)  // (-180, 180) / 8.4e-8 "
    v.setUint16(9, clamp(l.altitude * 16, 0, 65535))  // (0, ~4000) / 0.063 m
    v.setFloat64(11, l.time)
    v.setUint16(19, clamp(l.accuracy * 655, 0, 65535))  // (0, ~100) / 0.0015 m
    v.setUint16(21, l.bearing * 182)  // (0, 360) / 0.0055 °
    v.setUint16(23, clamp(l.speed * 1191, 0, 65535))  // (0, ~200) / 0.003 km/h
    v.setUint8(0, GPS)
  })
  
  sensors.gravity.onChange((g) => {
    var v = _get_data_view(13)
    v.setFloat32(1, g.x)
    v.setFloat32(5, g.y)
    v.setFloat32(9, g.z)
    v.setUint8(0, GRA)
  })
  
  sensors.linearAcceleration.onChange((a) => {
    var v = _get_data_view(13)
    v.setFloat32(1, a.x)
    v.setFloat32(5, a.y)
    v.setFloat32(9, a.z)
    v.setUint8(0, LAC)
  })
  
  tim_loop = util.loop(60e3, () => {
    var v = _get_data_view(9)
    v.setFloat64(1, (new Date()).getTime())
    v.setUint8(0, TIM)
  })
  
  sensors.accelerometer.start()
  sensors.gyroscope.start()
  sensors.location.start()
  sensors.gravity.start()
  sensors.linearAcceleration.start()
  tim_loop.start()
}


function clamp(a, low, high) {
  if (a < low) {
    return low
  }
  if (high < a) {
    return high
  }
  return a
}


function _stop_record_local() {
  log('stop recording local')
  
  sensors.accelerometer.stop()
  sensors.gyroscope.stop()
  sensors.location.stop()
  sensors.gravity.stop()
  sensors.linearAcceleration.stop()
  tim_loop.stop()
}


var BUF_SIZE = 1e3
var connection = undefined


function _start_record_espruino() {
  log('start recording Espruino')
  
  connection = espruino.connect(
    DEVICE_ADDRESS,
    () => log('connected to Espruino'),
    () => log('disconnected from Espruino'),
    (b) => log('Espruino: ' + javascript.get_string(new Uint8Array(b))),
    (e) => log('MTU change to ' + e.mtu + ' ' + (e.success ? 'succeeded' : 'FAILED'))
  )

  var array = new Uint8Array(BUF_SIZE)
  ; (new DataView(array.buffer)).setUint16(0, 0xffff);
  var offset = 0
  var session = new rack.Session(1, (a) => {
    if (a.length == 1) {
      if (offset == 0) {
        util.delay(5e3, () => rack.write(session, javascript.Uint8Array_subarray(array, 0, 2), javascript.pass))
        return

      } else if (offset == BUF_SIZE) {
        var v = _get_data_view(1 + BUF_SIZE)
        ; (new Uint8Array(v.buffer, v.byteOffset + 1, BUF_SIZE)).set(array)
        v.setUint8(0, ESP)

        offset = 0
        rack.write(session, javascript.Uint8Array_subarray(array, 0, 2), javascript.pass)
        return
      }
    }
    array.set(a, offset)
    offset += a.length
  }, (a) => {
    if (connection === undefined) session.stop = true
    else connection.write(a, rack.WRITE_ID, rack.SERVICE_ID)
  })

  connection.read(rack.READ_ID, rack.SERVICE_ID, (b) => rack._read(session, new Uint8Array(b)))
  rack.write(session, javascript.Uint8Array_subarray(array, 0, 2), javascript.pass)
}


function _stop_record_espruino() {
  log('stop recording Espruino')
  
  if (connection !== undefined) {
    connection.disconnect()
    connection = undefined
  }
}


function _collect() {
  if (collecting) return
  collecting = true
  
  if (recording) {
    log('cannot collect while recording')
    collecting = false
    return
  }
  
  log('collecting')

  var first_connect = true;

  connection = espruino.connect(
    DEVICE_ADDRESS,
    () => {
      log('connected to Espruino')
      if (first_connect) {
        first_connect = false
        connection.send_code('(function(){function d(){if(b){var a=b.read(490);if(a)Rack.write(c,new Uint8Array(E.toArrayBuffer(a)),d);else{var f=b;Rack.write(c,new Uint8Array([2]),function(){f.erase()});b=void 0}}}var e=require("rack"),b,c=Rack.Session(2,function(a){1==a[0]?Rack.write(c,new Uint8Array(E.toArrayBuffer(JSON.stringify(require("Storage").list(/^cyc/,{sf:!0})))),function(){Rack.write(c,new Uint8Array([1]),function(){})}):(b=require("Storage").open(E.toString(a),"r"),d(),d())},function(a){e.write(a)});e.sessions[2]=c})()\n')
      }
    },
    () => log('disconnected from Espruino'),
    (b) => log('Espruino: ' + javascript.get_string(new Uint8Array(b))),
    (e) => log('MTU change to ' + e.mtu + ' ' + (e.success ? 'succeeded' : 'FAILED'))
  )

  var string = ''
  var names
  var output_stream
  var start_date
  var byte_count = 0
  var progress_loop = util.loop(5e3, () => {
    ipc.send({
      id: 'set_collect_progress',
      elapsed_ms: start_date === undefined ? 0 : ((new Date()) - start_date),
      byte_count: byte_count,
      source: 'service',
    }, ACTIVITY_PORT)
  })
  progress_loop.start()

  var session = new rack.Session(2, (a) => {
    if (a[0] == 1) {  // end of list
      names = JSON.parse(string)
      next_file()

    } else if (a[0] == 2) {  // end of file
      output_stream.flush()
      output_stream.close()
      next_file()

    } else {
      if (output_stream) {
        output_stream.write(javascript.get_java_array(new Int8Array(a.buffer, a.byteOffset, a.byteLength)))
        byte_count += a.length

      } else {
        string += javascript.get_string(a)
      }
    }
  }, (a) => {
    connection.write(a, rack.WRITE_ID, rack.SERVICE_ID)
  })

  function next_file() {
    name = names.pop()
    if (name) {
      log('start transfering ' + name)
      if (start_date === undefined) start_date = new Date()
      output_stream = javascript.get_output_stream(name.replace(/:/g, '+'))
      rack.write(session, javascript.get_array(name), javascript.pass)

    } else {
      connection.send_code('require("rack").sessions[2] = undefined\n')
      util.delay(2e3, () => {
        connection.disconnect()
        connection = undefined
        progress_loop.stop()
        log('collected')
        collecting = false
      })
    }
  }

  connection.read(rack.READ_ID, rack.SERVICE_ID, (b) => rack._read(session, new Uint8Array(b)))
  rack.write(session, new Uint8Array([1]), javascript.pass)
}


var gps = false

function _gps(gps_) {
  if (gps_ == gps) return
  gps = gps_

  if (gps) {
    if (!recording) {
      var number = 0
      sensors.location.onSatellitesChange((s) => {
        if (s.satellitesInView != number) {
          number = s.satellitesInView
          log('GPS satellites: ' + number)
        }
      })

      sensors.location.onChange((g) => {
        log('GPS ready')
        device.vibrate(500)
        sensors.location.onSatellitesChange(javascript.pass)
        sensors.location.onChange(javascript.pass)
      })
    }
    sensors.location.start()
    log('enabled GPS')

  } else {
    sensors.location.onSatellitesChange(javascript.pass)
    sensors.location.stop()
    log('disabled GPS')
  }
}


onCreate()

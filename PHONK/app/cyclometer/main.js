function require(path) { }
require('../lib/phonk')

app.load('../lib/aes.js')
app.load('../lib/base64.js')
app.load('../lib/ipc.js')  // depends on aes, base64, javascript
app.load('../lib/javascript.js')
app.load('../lib/mui.js')


var PORT = 59953
var SERVICE_PORT = 59954
var AES_KEY = '96wySBkrRWF3AsQD+2VHbA=='


javascript.initialize(false)


var info = device.info()

var Y_MARGIN = 0.01
var X_MARGIN = Y_MARGIN * info.screenHeight / info.screenWidth
var BUTTON_HEIGHT = 0.05 * info.screenHeight


var divider = new android.graphics.drawable.ShapeDrawable()
divider.setAlpha(0)
divider.setIntrinsicWidth(info.screenWidth * X_MARGIN)
divider.setIntrinsicHeight(info.screenHeight * Y_MARGIN)

function create_linear_layout(divided) {
  if (divided === undefined) divided = true
  var view = ui.newView('linearLayout')
  view.props.padding = 0
  view.props.background = '#00000000'
  if (divided) {
    view.setDividerDrawable(divider)
    view.setShowDividers(android.widget.LinearLayout.SHOW_DIVIDER_MIDDLE)
  }
  return view
}


// main page
// - actions
var start_button = mui.text(ui.newView('button'), 'Start')
var stop_button = mui.text(ui.newView('button'), 'Stop')
var collect_button = mui.text(ui.newView('button'), 'Collect')
var test_button = mui.text(ui.newView('button'), 'T')
// - progress
var progress_layout = create_linear_layout(false)
progress_layout.props.background = start_button.props.background
progress_layout.props.padding = Y_MARGIN * info.screenHeight
var progress_text = mui.text(ui.newView('text'), 'Progress')
var first_progress_text = mui.text(ui.newView('text'), '0.0 min')
var second_progress_text = mui.setProps(ui.newView('text'), {textAlign: 'center'})
var third_progress_text = mui.setProps(ui.newView('text'), {textAlign: 'right'})
//
var text_list = (new mui.TextList()).textSize(13).autoScroll(true)
// - options
var scroll_toggle = mui.text(ui.newView('toggle'), 'Scroll', ['textOn', 'textOff'])
var wl_button = mui.text(ui.newView('button'), 'Wake')
var gps_toggle = mui.text(ui.newView('toggle'), 'GPS', ['textOn', 'textOff'])
// support page
var permissions_button = mui.text(ui.newView('button'), 'Permissions')


var pager = ui.addPager(0, 0, 1, 1)

var area = ui.newArea()

area.addView(mui.create_layout(['root', create_linear_layout(), {}, [
  ['header', mui.html(ui.newView('text'), '<b>Cyclometer</b> > <i>swipe</i>').textSize(11).textAlign(android.view.Gravity.CENTER), {width: -1}],
  ['actions', mui.LinearLayout_orientation(create_linear_layout(), 'horizontal'), {width: -1}, [
    ['start', start_button, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
    ['stop', stop_button, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
    ['collect', collect_button, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
    ['test', test_button, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
  ]],
  ['progress', progress_layout, {width: -1}, [
    ['header', progress_text, {weight: 0}],
    ['row', mui.LinearLayout_orientation(create_linear_layout(), 'horizontal'), {width: -1}, [
      ['first', first_progress_text, {width: 1, weight: 1}],
      ['second', second_progress_text, {width: 1, weight: 1}],
      ['third', third_progress_text, {width: 1, weight: 1}],
    ]],
  ]],
  ['log', text_list.get_list(), {weight: 1, width: -1}],
  ['options', mui.LinearLayout_orientation(create_linear_layout(), 'horizontal'), {width: -1}, [
    ['scroll', scroll_toggle, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
    ['wake', wl_button, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
    ['gps', gps_toggle, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
    ['space4', ui.newView('text'), {width: 1, weight: 1}],
  ]],
]]), X_MARGIN, Y_MARGIN, 1 - 2 * X_MARGIN, 1 - 2 * Y_MARGIN)

pager.add(area.getView())


area = ui.newArea()

area.addView(mui.create_layout(['root', create_linear_layout(), {}, [
  ['header', mui.html(ui.newView('text'), '<i>swipe</i> < <b>Support</b>').textSize(11).textAlign(android.view.Gravity.CENTER), {width: -1}],
  ['row', mui.LinearLayout_orientation(create_linear_layout(), 'horizontal'), {width: -1}, [
    ['permissions', permissions_button, {height: BUTTON_HEIGHT, width: 1, weight: 1}],
    ['space2', ui.newView('text'), {width: 1, weight: 1}],
    ['space3', ui.newView('text'), {width: 1, weight: 1}],
  ]],
]]), X_MARGIN, Y_MARGIN, 1 - 2 * X_MARGIN, 1 - 2 * Y_MARGIN)

pager.add(area.getView())


function log(string) {
  var date = new Date()
  text_list.add(date.toISOString().substring(11, 23) + ' ' + string + '\n')
}


function onCreate() {
  device.screenAlwaysOn(true)

  ipc.start(PORT, base64.decode(AES_KEY), _on_receive)

  ipc.send({id: 'get_state', source: 'activity'}, SERVICE_PORT, javascript.pass, (response) => {
    log('ERROR service responded ' + response)
  })

  if (app.getContext().checkSelfPermission(android.Manifest.permission.ACCESS_BACKGROUND_LOCATION) != android.content.pm.PackageManager.PERMISSION_GRANTED) {
    log('Currently, location permission for this app is NOT set to "Allow all the time". Therefore, GPS is unavailable while the screen is off / device is locked. To grant this permission, you have to navigate to the corresponding settings page. The button "Permissions" on the second page (swipe left) gets you close.')
  }
}


function onPause() {
  device.screenAlwaysOn(false)
}


function onResume() {
  device.screenAlwaysOn(true)
}


function onDestroy() {
  ipc.stop()

  device.screenAlwaysOn(false)
}


var wl_change_id


function _on_receive(object) {
  if (object.source == 'service') {
    if (object.id == 'log') {
      log('service: ' + object.string)
      
    } else if (object.id == 'set_state') {
      wl_button.props.background = object.state.wakelock ? '#ff669900' : '#ffcc0000'
      
      log('connected to service')
      if (object.state.recording) {
        log('service is recording')
      }
      
    } else if (object.id == 'set_wakelock') {
      if (object.change_id == wl_change_id) {
        wl_button.props.background = object.wakelock ? '#ff669900' : '#ffcc0000'
      }

    } else if (object.id == 'set_record_progress') {
      mui.text(progress_text, 'Recorded')
      mui.text(first_progress_text, (object.elapsed_ms / 60e3).toFixed(1) + ' min')
      mui.text(second_progress_text, _get_readable(object.byte_count / object.elapsed_ms * 60e3) + '/min')
      mui.text(third_progress_text, _get_readable(object.byte_count))

    } else if (object.id == 'set_collect_progress') {
      mui.text(progress_text, 'Collected')
      mui.text(first_progress_text, (object.elapsed_ms / 60e3).toFixed(1) + ' min')
      mui.text(second_progress_text, _get_readable(object.byte_count / object.elapsed_ms * 60e3) + '/min')
      mui.text(third_progress_text, _get_readable(object.byte_count))
    }
  }
}


function _get_readable(byte_count) {
  if (isNaN(byte_count)) return '0.0 B'
  var factor
  var unit
  if (byte_count < 1e3) {
    factor = 1
    unit = 'B'
  } else if (byte_count < 1e6) {
    factor = 1e3
    unit = 'kB'
  } else {
    factor = 1e6
    unit = 'MB'
  }
  return (byte_count / factor).toFixed(1) + ' ' + unit
}


scroll_toggle.onChange((o) => {
  text_list.autoScroll(o.checked)
})
scroll_toggle.checked(true)


wl_button.onClick(() => {
  wl_button.props.background = '#00000000'
  wl_change_id = Math.random()
  ipc.send({id: 'toggle_wakelock', change_id: wl_change_id, source: 'activity'}, SERVICE_PORT)
})


start_button.onClick(() => {
  ipc.send({id: 'start_recording', source: 'activity'}, SERVICE_PORT)
})


stop_button.onClick(() => {
  ipc.send({id: 'stop_recording', source: 'activity'}, SERVICE_PORT)
})


collect_button.onClick(() => {
  ipc.send({id: 'collect', source: 'activity'}, SERVICE_PORT)
})


gps_toggle.onChange((o) => {
  ipc.send({id: 'gps', gps: o.checked, source: 'activity'}, SERVICE_PORT)
})


permissions_button.onClick(() => {
  var intent = new android.content.Intent()
  intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
  intent.setData(android.net.Uri.parse('package:' + app.getContext().getPackageName()))
  intent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK)
  device.getContext().startActivity(intent)
})


test_button.onClick(() => {
  ipc.send({id: 'test', source: 'activity'}, SERVICE_PORT)
})

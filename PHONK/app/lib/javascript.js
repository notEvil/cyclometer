var javascript = (() => {
  var is_service


  function pass() {}
  
  
  function initialize(is_service_) {
    is_service = is_service_
  }
  
  
  function get_java_array(int8_array) {
    var java_array = java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, int8_array.length)
    for (var index in int8_array) {
      java_array[index] = int8_array[index]
    }
    return java_array
  }
  
  
  function get_int8_array(java_array) {
    var int8_array = new Int8Array(java_array.length)
    for (var index in int8_array) {
      int8_array[index] = java_array[index]
    }
    return int8_array
  }
  
  
  function main_thread(inner_function) {
    return function() {
      var function_ = () => inner_function.apply(null, arguments)
      if (is_service) {
        app.runOnMainLoop(function_)
      } else {
        app.runOnUiThread(function_)
      }
    }
  }
  
  
  function get_output_stream(path_string) {
    var path = app.fullPath() + path_string
    var file = new java.io.File(path)
    if (!file.exists()) file.createNewFile()
    return new java.io.FileOutputStream(file)
  }


  // https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
  function get_array(string) {
    var array = new Uint8Array(string.length)
    for (var index = 0; index < string.length; index++) {
      array[index] = string.charCodeAt(index)
    }
    return array
  }
  
  
  // https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
  function get_string(array) {
    return String.fromCharCode.apply(null, array)
  }


  function Int8Array_subarray(array, start, stop) {
    return _subarray(Int8Array, array, start, stop)
  }

  function Uint8Array_subarray(array, start, stop) {
    return _subarray(Uint8Array, array, start, stop)
  }

  function _subarray(type, array, start, stop) {
    if (array.length <= start) return new type(0)
    if (stop === undefined || array.length < stop) {
      stop = array.length
    }
    return new type(array.buffer, array.byteOffset + start, stop - start)
  }


  return {
    pass: pass,
    initialize: initialize,
    get_java_array: get_java_array,
    get_int8_array: get_int8_array,
    main_thread: main_thread,
    get_output_stream: get_output_stream,
    get_array: get_array,
    get_string: get_string,
    Int8Array_subarray: Int8Array_subarray,
    Uint8Array_subarray: Uint8Array_subarray
  }
})()

// depends on
// - javascript


var zstd = (() => {
  function get_output_stream(output_stream) {
    return new com.github.luben.zstd.ZstdOutputStream(output_stream)
  }
  
  
  function test() {
    var output_stream = javascript.get_output_stream('test')
    var zstd_output_stream = get_output_stream(output_stream)
    zstd_output_stream.write(java.lang.String('test').getBytes())
    zstd_output_stream.flush()
    zstd_output_stream.close()
    output_stream.flush()
    output_stream.close()
    log('here')
  }


  return {get_output_stream: get_output_stream, test: test}
})()

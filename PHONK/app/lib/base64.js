var base64 = (() => {
  function encode(java_array) {
    return java.lang.String(java.util.Base64.getEncoder().encode(java_array))
  }
  
  
  function decode(string) {  // -> java array
    return java.util.Base64.getDecoder().decode(string)
  }
  
  
  return {encode: encode, decode: decode, atob: decode, btoa: encode}
})()

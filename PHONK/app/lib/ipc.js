// depends on
// - aes
// - base64
// - javascript


var ipc = (() => {
  var http_server
  var aes_secret


  function encode(object) {
    return base64.encode(aes.encrypt(java.lang.String(JSON.stringify(object)).getBytes(), aes_secret))
  }


  function decode(string) {
    string = java.lang.String(aes.decrypt(base64.decode(string), aes_secret))
    if (string == 'undefined') return
    return JSON.parse(string)
  }
  
  
  function start(port, aes_key, on_receive) {
    on_receive = javascript.main_thread(on_receive)
    
    http_server = network.createHttpServer(null, port)
    aes_secret = aes.get_secret(aes_key)
    
    http_server.onNewRequest((request) => {
      if (!(request.uri == '/' && request.method == 'POST')) {
        return
      }
      var array = request.params.object
      if (array === undefined || array.length != 1) {
        return
      }

      on_receive(decode(array[0]))
      return http_server.response('')
    })
    
    http_server.start()
  }
  
  
  function stop() {
    http_server.stop()
    http_server = undefined
    aes_secret = undefined
  }
  
  
  function send(object, port, on_success, on_failure) {
    if (aes_secret === undefined) {
      if (on_failure !== undefined) on_failure()
      return
    }

    network.httpRequest({
      data: [{name: 'object', content: encode(object), type: 'text'}],
      url: ('http://127.0.0.1:' + port + '/').toString(),
      method: 'POST',
    }).onResponse((response) => {
      if (response.status == 200) {
        if (on_success !== undefined) on_success()
      } else {
        if (on_failure !== undefined) on_failure(response)
      }
    })
  }
  
  
  return {start: start, stop: stop, send: send}
})()

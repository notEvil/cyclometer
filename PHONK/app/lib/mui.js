var mui = (() => {
  var TextList = function() {
    this._list = ui.newView('list')
    this._lines = []

    this._list.init(this._lines, () => this._create_text(), (o) => this._set_text(o))
  }


  TextList.prototype.get_list = function() {
    return this._list
  }


  TextList.prototype._create_text = function() {
    var text = ui.newView('text')
    if (this._text_font !== undefined) {
      text.textFont(this._text_font)
    }
    if (this._text_size !== undefined) {
      text.textSize(this._text_size)
    }
    return text
  }


  TextList.prototype._set_text = function(o) {
    o.view.text(this._lines[o.position])
  }


  TextList.prototype.textSize = function(number) {
    this._text_size = number
    return this
  }


  TextList.prototype.autoScroll = function(bool) {
    this._auto_scroll = bool
    return this
  }


  TextList.prototype.textFont = function(object) {
    this._text_font = object
    return this
  }


  TextList.prototype.add = function(string) {
    if (this._lines.length != 0) {
      string = this._lines.pop() + string
    }
    this._lines.push.apply(this._lines, string.split('\n'))
    this._list.notifyDataChanged()
    if (this._auto_scroll === true) {
      this._list.scrollToPosition(this._lines.length - 1)
    }
    return this
  }


  function create_layout(definition, parent) {
    var view = definition[1]
    var sub_definitions = definition[3]
    if (sub_definitions !== undefined) {
      for (var sub_definition of sub_definitions) {
        create_layout(sub_definition, view)
      }
    }
    if (parent !== undefined) {
      var options = definition[2]
      if (options === undefined) options = {}
      var width = options.width
      if (width === undefined) width = android.view.ViewGroup.LayoutParams.WRAP_CONTENT
      var height = options.height
      if (height === undefined) height = android.view.ViewGroup.LayoutParams.WRAP_CONTENT
      var weight = options.weight
      if (weight === undefined) weight = 0
      parent.addView(view, new android.widget.LinearLayout.LayoutParams(width, height, weight))  // TODO add to .mViews
    }
    return view
  }


  function LinearLayout_orientation(linear_layout, orientation) {
    linear_layout.orientation(orientation)
    return linear_layout
  }


  function setProps(view, props) {
    view.setProps(props)
    return view
  }


  function text(view, text, names) {
    view.text(text)
    view.props.text = text
    if (names !== undefined) {
      for (var name of names) {
        view.props[name] = text
      }
    }
    return view
  }


  function html(view, html) {
    view.html(html)
    view.props.text = view.getText()
    return view
  }


  return {
    TextList: TextList,
    create_layout: create_layout,
    LinearLayout_orientation: LinearLayout_orientation,
    setProps: setProps,
    text: text,
    html: html,
  }
})()

var aes = (() => {
  function get_secret(java_array) {
    return new javax.crypto.spec.SecretKeySpec(java_array, 'AES')
  }
  
  
  function encrypt(java_array, secret) {
    var Cipher = javax.crypto.Cipher
    var cipher = Cipher.getInstance('AES/ECB/PKCS5Padding')
    cipher.init(Cipher.ENCRYPT_MODE, secret)
    return cipher.doFinal(java_array)
  }
  
  
  function decrypt(java_array, secret) {
    var Cipher = javax.crypto.Cipher
    var cipher = Cipher.getInstance('AES/ECB/PKCS5Padding')
    cipher.init(Cipher.DECRYPT_MODE, secret)
    return cipher.doFinal(java_array)
  }
  
  
  return {get_secret: get_secret, encrypt: encrypt, decrypt: decrypt}
})()

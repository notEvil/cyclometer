// depends on
// - javascript


var espruino = (() => {
  var _MTU = 53
  var _MSG_SIZE = _MTU - 3
  var _NORDIC_SERVICE = '6e400001-b5a3-f393-e0a9-e50e24dcca9e'
  var _NORDIC_TX = '6e400002-b5a3-f393-e0a9-e50e24dcca9e'
  var _NORDIC_RX = '6e400003-b5a3-f393-e0a9-e50e24dcca9e'
  
  
  function connect(address, on_connected, on_disconnected, on_new_data, on_mtu_changed) {
    network.bluetoothLE.start()
    
    var connection = new _Connection(address)

    connection.ble_client.onNewDeviceStatus((event) => {
      if (event.status == 'connected') {
        for (var service_id in connection.read_functions) {
          for (var characteristic_id in connection.read_functions[service_id]) {
            connection.ble_client.readFromCharacteristic(address, service_id, characteristic_id)
          }
        }
        on_connected()
      } else if (event.status == 'disconnected') {
        on_disconnected()
      }
    })
    connection.ble_client.onNewData((event) => {
      var array_buffer = javascript.get_int8_array(event.value).buffer
      for (var function_ of connection.read_functions[event.serviceUUID][event.characteristicUUID]) {
        function_(array_buffer)
      }
    })
    if (on_mtu_changed !== undefined) {
      connection.ble_client.onMtuChanged(on_mtu_changed)
    }
    
    connection.ble_client.autoConnect(true)
    connection.read(_NORDIC_RX, _NORDIC_SERVICE, on_new_data)
    
    connection.ble_client.connectDevice(address, _MTU)
    return connection
  }
  
  
  var _Connection = function(address) {
    this.address = address
    this.ble_client = network.bluetoothLE.createClient()
    this.read_functions = {}
  }
  
  
  _Connection.prototype.disconnect = function(on_success, on_failure) {
    this.ble_client.autoConnect(false)
    this.ble_client.disconnectDevice(this.address)
    if (on_success !== undefined) util.delay(3000, on_success)
  }
  
  
  _Connection.prototype.send_code = function(string) {
    this.write(javascript.get_array(_transform_code(string)), _NORDIC_TX, _NORDIC_SERVICE)
  }
  
  
  function _transform_code(string) {
    return '\x03\x10' + string.substring(0, string.length - 1).replace(/\n/g, '\n\x10') + '\n'
  }


  _Connection.prototype.read = function(characteristic_id, service_id, function_) {
    _setdefault(_setdefault(this.read_functions, service_id, {}), characteristic_id, []).push(function_)
    this.ble_client.readFromCharacteristic(this.address, service_id, characteristic_id)
  }


  function _setdefault(object, key, default_) {
    var value = object[key]
    if (value === undefined) {
      object[key] = default_
      return default_
    }
    return value
  }
  
  
  _Connection.prototype.write = function(array, characteristic_id, service_id) {
    array = new Int8Array(array.buffer, array.byteOffset, array.byteLength)
    while (array.length != 0) {
      this.ble_client._write(javascript.get_java_array(javascript.Int8Array_subarray(array, 0, _MSG_SIZE)), this.address, service_id, characteristic_id)
      array = javascript.Int8Array_subarray(array, _MSG_SIZE)
    }
  }


  return {connect: connect}
})()

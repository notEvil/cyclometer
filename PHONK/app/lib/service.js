var service = (() => {
  var MARGIN = 0.03
  var text_list
  
  function initialize(as_activity) {
    if (as_activity === undefined) {
      as_activity = false
    }
    
    if (as_activity) {
      device.screenAlwaysOn(true)

      text_list = ui.addTextList(MARGIN, MARGIN, 1 - 2*MARGIN, 1 - 2*MARGIN)
      text_list.autoScroll(true)
    }
  }
  
  function log(string) {
    if (text_list !== undefined) {
      text_list.add(string)
    }
  }
  
  return {initialize: initialize, log: log}
})()
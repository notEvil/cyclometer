# [*PHONK*](https://github.com/victordiaz/PHONK)

is "A Javascript coding playground for new and old Android devices." ([https://phonk.app/](https://phonk.app/)) And amazing at that, check it out!

For now, this project requires a custom version of *PHONK* because the current release version doesn't contain [https://github.com/victordiaz/PHONK/pull/136](https://github.com/victordiaz/PHONK/pull/136) which is essential.

## Details

### GPS

usually takes a while to start, depending on recent usage or available assistance. Therefore, you can turn on GPS using the corresponding button and wait for the first fix before pressing "Start". Additionally, you can turn off GPS after, if desired. This doesn't affect [../Bangle.js](../Bangle.js).

### Service

Android doesn't allow foreground processes (aka Activities) to keep running while the screen is off/device is locked. For this reason, `cyclometer` is just an interface with a few buttons and a message log. It communicates with the background service `cyclometer_service` which essentially does everything. Force closing the Activity doesn't stop the service. You have to either use the corresponding notification or force stop *PHONK* to achieve that.

### IPC

`cyclometer` communicates with `cyclometer_service` using HTTP POST requests carrying Base64 encoded AES encrypted JSON objects; see `lib/ipc.js`, `lib/base64.js` and `lib/aes.js`. Imo, this is easier than native/recommended methods.

### [*Espruino*](https://github.com/espruino/Espruino)

`lib/espruino.js` provides a high-level interface to devices running *Espruino*.

### Zstandard

This project used to compress data using [Zstd-jni](https://github.com/luben/zstd-jni); see `lib/zstd.js`. It is still there for future use cases.

### Custom `TextList`

Essentially, this is `ui.addTextList` reimplemented in Javascript; see `lib/mui.js`. The official version showed weird font size behavior which I couldn't solve. TODO report
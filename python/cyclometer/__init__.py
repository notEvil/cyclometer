import base64
import collections.abc as c_abc
import dataclasses
import datetime
import enum
import math
import struct


_ANDROID_BUFFER_LENGTH = 100 * 1000
_ESPRUINO_BUFFER_LENGTH = 1000


class _AndroidId(enum.Enum):
    ACC = 1
    GYR = 2
    GPS = 3
    GRA = 4
    LAC = 5
    ESP = 6
    TIM = 7


@dataclasses.dataclass
class AndroidAcc:
    ID = _AndroidId.ACC
    x: float
    y: float
    z: float


@dataclasses.dataclass
class AndroidGyr:
    ID = _AndroidId.GYR
    x: float
    y: float
    z: float


@dataclasses.dataclass
class AndroidGps:
    ID = _AndroidId.GPS
    longitude: float
    latitude: float
    altitude: float
    time: datetime.datetime
    accuracy: float
    bearing: float
    speed: float


@dataclasses.dataclass
class AndroidGra:
    ID = _AndroidId.GRA
    x: float
    y: float
    z: float


@dataclasses.dataclass
class AndroidLAc:
    ID = _AndroidId.LAC
    x: float
    y: float
    z: float


@dataclasses.dataclass
class AndroidEspruino:
    ID = _AndroidId.ESP
    id: int | None
    bytes: bytes


@dataclasses.dataclass
class AndroidTim:
    ID = _AndroidId.TIM
    time: datetime.datetime


def decode_android(file, start=None, stop=None, out_file=None):
    initial_position = file.tell()
    position = initial_position

    if start is not None:
        position = initial_position + start * _ANDROID_BUFFER_LENGTH
        file.seek(position)

    _ = stop is None
    stop_position = None if _ else (initial_position + stop * _ANDROID_BUFFER_LENGTH)

    if out_file is None:

        def read(length, replace=None):
            nonlocal position
            position += length
            return file.read(length)

    else:

        def read(length, replace=None):
            nonlocal position
            position += length
            bytes = file.read(length)
            out_file.write(bytes if replace is None else replace(bytes))
            return bytes

    first_espruino_id = None

    def replace_espruino_id(bytes):
        nonlocal first_espruino_id

        id = struct.unpack(">H", bytes)[0]
        if first_espruino_id is None:
            first_espruino_id = id

        return struct.pack(">H", id - first_espruino_id)

    while True:
        if stop_position is not None and stop_position <= position:
            break

        id_bytes = read(1)
        if len(id_bytes) == 0:
            break

        id = id_bytes[0]

        if id == 0:
            _ = math.ceil((position - initial_position) / _ANDROID_BUFFER_LENGTH)
            next_position = initial_position + _ * _ANDROID_BUFFER_LENGTH
            if out_file is None:
                file.seek(next_position)
                position = next_position

            else:
                read(next_position - position)

            continue

        match _AndroidId(id):
            case _AndroidId.ACC:
                x, y, z = struct.unpack(">fff", read(12))
                yield AndroidAcc(x=x, y=y, z=z)

            case _AndroidId.GYR:
                x, y, z = struct.unpack(">fff", read(12))
                yield AndroidGyr(x=x, y=y, z=z)

            case _AndroidId.GPS:
                (
                    latitude,
                    longitude,
                    altitude,
                    time,
                    accuracy,
                    bearing,
                    speed,
                ) = struct.unpack(">iiHdHHH", read(24))
                yield AndroidGps(
                    longitude=longitude / 11930464,
                    latitude=latitude / 11930464,
                    altitude=altitude / 16,
                    time=datetime.datetime.utcfromtimestamp(time / 1000),
                    accuracy=accuracy / 655,
                    bearing=bearing / 182,
                    speed=speed / 1191,
                )

            case _AndroidId.GRA:
                x, y, z = struct.unpack(">fff", read(12))
                yield AndroidGra(x=x, y=y, z=z)

            case _AndroidId.LAC:
                x, y, z = struct.unpack(">fff", read(12))
                yield AndroidLAc(x=x, y=y, z=z)

            case _AndroidId.ESP:
                _ = struct.unpack(">H", read(2, replace=replace_espruino_id))
                yield (
                    AndroidEspruino(id=_[0], bytes=read(_ESPRUINO_BUFFER_LENGTH - 2))
                )

            case _AndroidId.TIM:
                (time,) = struct.unpack(">d", read(8))
                yield AndroidTim(time=datetime.datetime.utcfromtimestamp(time / 1000))

            case _:
                raise Exception(id)


def decode(android_file, espruino_file):
    def _():
        bytes_length = math.ceil(_ESPRUINO_BUFFER_LENGTH / 3) * 4  # base64
        while True:
            bytes = espruino_file.read(bytes_length)
            if len(bytes) != bytes_length:
                break

            bytes = base64.b64decode(bytes)
            yield AndroidEspruino(id=struct.unpack(">H", bytes[:2])[0], bytes=bytes[2:])

    espruino_objects = iter(_())
    espruino_object = next(espruino_objects, None)

    for object in decode_android(android_file):
        if isinstance(object, AndroidEspruino):
            while espruino_object is not None and espruino_object.id < object.id:
                yield espruino_object
                espruino_object = next(espruino_objects, None)

        yield object

    while espruino_object is not None:
        yield espruino_object
        espruino_object = next(espruino_objects, None)


class _EspruinoId(enum.Enum):
    ACC = 1
    MAG = 2
    GPS = 3
    TIM = 4


@dataclasses.dataclass
class EspruinoAcc:
    ID = _EspruinoId.ACC
    x: float
    y: float
    z: float


@dataclasses.dataclass
class EspruinoMag:
    ID = _EspruinoId.MAG
    x: int
    y: int
    z: int


@dataclasses.dataclass
class EspruinoGps:
    ID = _EspruinoId.GPS
    longitude: float
    latitude: float
    altitude: float
    time: datetime.datetime
    fix: int
    hdop: float
    satellites: int
    course: float
    speed: float


@dataclasses.dataclass
class EspruinoTim:
    ID = _EspruinoId.TIM
    time: datetime.datetime


def decode_espruino(bytes: c_abc.Iterable[bytes]):
    for bytes in bytes:
        index = 0

        while True:
            if index == len(bytes) or bytes[index] == 0:
                break

            match _EspruinoId(bytes[index]):
                case _EspruinoId.ACC:
                    x, y, z = struct.unpack(">hhh", bytes[index + 1 : index + 7])
                    yield EspruinoAcc(x=x / 8192, y=y / 8192, z=z / 8192)
                    index += 7

                case _EspruinoId.MAG:
                    x, y, z = struct.unpack(">hhh", bytes[index + 1 : index + 7])
                    yield EspruinoMag(x=x, y=y, z=z)
                    index += 7

                case _EspruinoId.GPS:
                    (
                        latitude,
                        longitude,
                        altitude,
                        time,
                        fix,
                        hdop,
                        satellites,
                        course,
                        speed,
                    ) = struct.unpack(">iiHdBBBHH", bytes[index + 1 : index + 26])
                    yield EspruinoGps(
                        longitude=longitude / 11930464,
                        latitude=latitude / 11930464,
                        altitude=altitude / 16,
                        time=datetime.datetime.utcfromtimestamp(time / 1000),
                        fix=fix,
                        hdop=hdop / 12,
                        satellites=satellites,
                        course=course / 182,
                        speed=speed / 327,
                    )
                    index += 26

                case _EspruinoId.TIM:
                    (time,) = struct.unpack(">d", bytes[index + 1 : index + 9])
                    yield EspruinoTim(time=datetime.datetime.utcfromtimestamp(time))
                    index += 9

                case _:
                    raise Exception


def align_objects(objects, type, time_type):
    first_time = None
    last_time = None
    count = 0
    first_count = 0
    total_count = 0

    for object in objects:
        if isinstance(object, time_type):
            if first_time is None:
                first_time = object.time
                first_count = count

            else:
                total_count += count

            last_time = object.time
            count = 0

        elif isinstance(object, type):
            count += 1

    time_delta = (last_time - first_time) / total_count
    return (first_time - first_count * time_delta, time_delta)


def get_data_frame(objects, first_time, time_delta):
    import pandas

    data_frame = pandas.DataFrame(objects)
    _ = len(data_frame.index) * time_delta
    data_frame.index = pandas.date_range(
        start=first_time, end=first_time + _, periods=len(data_frame.index)
    )
    return data_frame

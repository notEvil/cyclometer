# Python

This directory contains

1. the Python package `cyclometer` providing functions to decode data files
2. an example, consisting of
    - a [*Jupyter*](https://jupyter.org) notebook
    - the [*Pipenv*](https://github.com/pypa/pipenv) environment for the *Jupyter* notebook
    - example data

## Quickstart

- Optionally
    - record some data
    - get the data
- Install *Pipenv* with `python -m pip install pipenv`
- Navigate to this directory
- Set up *Pipenv* environment with `python -m pipenv install`
- Start *JupyterLab* with `python -m pipenv run python -m jupyter lab`
- Optionally
    - set `android_file_path` and `espruino_file_path`
- Execute all cells from top to bottom

If you are familiar with data processing/analysis in Python, you should be ready to start.
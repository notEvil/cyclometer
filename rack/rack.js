var MTU = 53;
var MSG_SIZE = MTU - 3;
var PART = MSG_SIZE - 1;
var WD_MS = 2 * 1000;


var Session = function(id, rfun, wfun) {
  this.id = id;
  this.rfun = rfun;
  this.wfun = wfun;

  this.wque = [];
  this.wcnt = 0;
  this.widx = 0;
  this.woff = 0;
  this.wid = 0;
  this.aid = 0;
  this.aoff = 0;
  this.rid = 0;
}


Session.prototype.write = function(arr, cbf) {
  this.wque.push(arr, cbf);
  while (this.wcnt < 4 && this._wrt()) this.wcnt++;
  if (!this.wdt) {
    this.wdd = new Date();
    this.wdt = setTimeout(this._wd.bind(this), WD_MS);
  }
}


function write(sess, arr, cbf) {
  sess.write(arr, cbf);
}


Session.prototype._read = function(arr) {
  if (arr.length == 1) {  // ack
    if ((arr[0] & 0xf) != (this.aid & 0xf)) return;
    this.aid++;
    var o = this.aoff + PART;
    var q = this.wque;
    if (q[0].length <= o) {
      q.splice(0, 2)[1]();
      this.widx -= 2;
      o = 0;
    }
    this.aoff = o;
    if (!this._wrt()) this.wcnt--;
    this.wdd = new Date();
  } else {
    this.wfun(arr.subarray(0, 1));  // ack
    if ((arr[0] & 0xf) != (this.rid & 0xf)) return;
    this.rfun(arr.subarray(1));
    this.rid++;
  }
}


function _read(sess, arr) {
  sess._read(arr);
}


Session.prototype._wd = function() {
  var ms = (new Date()) - this.wdd;
  if (ms < WD_MS) {
    this.wdt = setTimeout(this._wd.bind(this), WD_MS - ms);
  } else if (this.wque.length) {
    this.wcnt = 0;
    this.widx = 0;
    this.woff = this.aoff;
    this.wid = this.aid;
    while (this.wcnt < 4 && this._wrt()) this.wcnt++;
    this.wdt = setTimeout(this._wd.bind(this), WD_MS);
  } else {
    this.wdt = undefined;
  }
}


Session.prototype._wrt = function() {
  var q = this.wque;
  var i = this.widx;
  if (q.length <= i) return false;
  var o = this.woff;
  var arr = q[i];
  var msg = new Uint8Array(1 + Math.min(o + PART, arr.length) - o);
  msg[0] = (this.id << 4) | (this.wid++ & 0xf);
  msg.set(arr.subarray(o, o + msg.length - 1), 1);
  this.wfun(msg);
  o += PART;
  if (arr.length <= o) {
    this.widx += 2;
    o = 0;
  }
  this.woff = o;
  return true;
}


function read(arr, sess) {
  sess = sess[arr[0] >> 4];
  if (!sess) return;
  _read(sess, arr);
}


exports.Session = Session
exports.write = write
exports._read = _read
exports.read = read

# *Rack*

short for **r**oute and **ack**nowledge, is an algorithm for multi-tenant and robust communication. It solves two issues related to communication over Bluetooth:

1. Bluetooth doesn't provide low complexity routing to separate messages from different parts of the application (similar to UDP ports).

There are GATT Characteristics, but managing/using them is complex. The official applications for [*Espruino*](https://github.com/espruino/Espruino) use the Nordic UART to execute code and capture the output which necessitates single-tenancy.

2. Bluetooth provides a robust link layer but the application can't determine if a message was received or not (similar to UDP).

A common solution to this problem, as found in the official applications for *Espruino*, is application level validation and retry on timeout. This increases the complexity of the application unnecessarily.

## Quickstart

You can
- read [./rack.js](./rack.js)
- execute `node ./test.js`
- build [Bangle.js/Espruino](../Bangle.js/Espruino) for `LINUX` (adjust and execute [build.py](../Bangle.js/Espruino/build.py)) and execute `/path/to/espruino ./test.js`
- read corresponding parts in [PHONK/app/cyclometer_service/main.js](../PHONK/app/cyclometer_service/main.js), [Bangle.js/app/cyc.app.js](../Bangle.js/app/cyc.app.js) and [PHONK/app/lib/rack.js](../PHONK/app/lib/rack.js), [Bangle.js/app/rack.boot.js](../Bangle.js/app/rack.boot.js), [Bangle.js/app/rack.js](../Bangle.js/app/rack.js), [https://github.com/notEvil/Espruino/tree/rack](https://github.com/notEvil/Espruino/tree/rack)

## How

*Rack* provides sessions identified by numbers. Each session manages its own state and

- on send request (`write`), enqueues the array to send and function to call
- splits arrays into messages
- on send message (`_wrt`), prepends the message with the session id and consecutively numbered message id
- sends up to 4 messages before waiting for an acknowledge
- on receive message (`_read`)
    - if its a message
        - sends acknowledge
        - if message id matches the expected id
            - calls a function passing the message (`rfun`)
    - if its an acknowledge
        - if message id matches the expected id
            - if it was the last message of the array
                - dequeues the array and function
                - calls the function
            - sends the next message
- if no acknowledge was received within 2 seconds, sends up to 4 messages again (`_wd`)

Sessions are added to an array which is used to route messages to the corresponding session (`read`).

## Details

**Bluetooth MTU**

At the moment, MTU must not drop below a fixed number (53) during operation. This is usually not a problem since MTU is negotiated after the connection is established and doesn't change thereafter.

**Burst**

The algorithm sends up to 4 messages in one go for maximum throughput. This number is entirely arbitrary and may be adjusted if it causes issues (e.g. buffer overflow with many concurrent sessions).

**Buffered version**

Originally, this algorithm used read and write buffers to handle out of order transmission/loss. It was slightly more complex and had higher memory requirements. If anyone is interested, I could add it to this repository.
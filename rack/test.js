// is Espruino
var esp;
try {
  E;
  esp = true;
} catch (e) {
  esp = false;
}

// load Rack
try {
  Rack;  // C version
} catch (e) {
  Rack = require(esp ? 'rack' : './rack.js');  // JS version
}


var MTU = 53;
var MSG_SIZE = MTU - 3;
var PART = MSG_SIZE - 1;


function get_array() {
  var array = new Uint8Array(Math.round(1 + Math.random() * (10 * PART)));  // [1, 10 * PART] bytes
  for (var index in array) {
    array[index] = Math.round(Math.random() * 255);  // [0, 255]
  }
  return array;
}


function compare(array, expected) {
  if (!(array.length <= expected.length)) return false;
  for (var index in array) {
    if (array[index] != expected[index]) return false;
  }
  return true;
}


var expected = [];
var error = false;

var wp1 = new Promise((resolve) => resolve());
var wp2 = new Promise((resolve) => resolve());
var mcnt = 0;

var sessions_1 = [];
var sessions_2 = [];
sessions_1[1] = new Rack.Session(
  1,
  (arr) => {  // read
    error = true;
    console.log('E: session_1 read');
  },
  (arr) => {  // write
    var p = Math.random();
    if (p < 0.979) {  // instant
      wp1 = wp1.then(() => new Promise((resolve) => setTimeout(() => { mcnt++; Rack.read(arr, sessions_2); resolve(); }, Math.random() * 10)));  // [0, 10] ms
    } else if (p < 0.989) {  // delayed; 1 %
      wp1 = wp1.then(() => new Promise((resolve) => setTimeout(() => { mcnt++; Rack.read(arr, sessions_2); resolve(); }, Math.random() * 1000)));  // [0, 1] s
    } else if (p < 0.999) {  // loss; 1 %
      console.log('loss');
    } else {  // loss and disconnect/reconnect; 0.1 %
      wp1 = wp1.then(() => { console.log('disconnect'); return new Promise((resolve) => setTimeout(resolve, 5 * 1000)); });  // 5 s
    }
  }
);
sessions_2[1] = new Rack.Session(
  1,
  (arr) => {  // read
    if (expected.length == 0) {
      error = true;
      console.log('E: no expect');
    } else {
      var earr = expected[0];
      if (compare(arr, earr)) {
        if (arr.length == earr.length) {
          expected.shift();
        } else {
          expected[0] = earr.subarray(arr.length);
        }
      } else {
        error = true;
        console.log('E: compare');
      }
    }
  },
  (arr) => {  // write
    var p = Math.random();
    if (p < 0.979) {  // instant
      wp1 = wp1.then(() => new Promise((resolve) => setTimeout(() => { mcnt++; Rack.read(arr, sessions_1); resolve(); }, Math.random() * 10)));  // [0, 10] ms
    } else if (p < 0.989) {  // delayed; 1 %
      wp1 = wp1.then(() => new Promise((resolve) => setTimeout(() => { mcnt++; Rack.read(arr, sessions_1); resolve(); }, Math.random() * 1000)));  // [0, 1] s
    } else if (p < 0.999) {  // loss; 1 %
      console.log('loss');
    } else {  // loss and disconnect/reconnect; 0.1 %
      wp1 = wp1.then(() => { console.log('disconnect'); return new Promise((resolve) => setTimeout(resolve, 5 * 1000)); });  // 5 s
    }
  }
);

var acnt = 0;

function next() {
  if (expected.length == 0 || Math.random() < 0.1) {
    var arr = get_array();
    expected.push(arr);
    Rack.write(sessions_1[1], arr, () => {});
    acnt++;
  }

  if (!error) setTimeout(next, Math.random() * 100);  // [0, 100] ms
}
next();

var lid = setInterval(() => {
  console.log('arrays: ' + acnt + ' queue: ' + expected.length + ' messages: ' + mcnt);
  if (esp) {
    var m = process.memory(false);
    var usage = m.usage;
    for (var i in expected) {  // remove expected
      usage -= E.getSizeOf(expected[i]);
    }
    console.log('memory: ' + (usage / m.total * 100) + ' %');
  }
  if (error) clearInterval(lid)
}, 1000);

